﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazookaBulletBehavior : MonoBehaviour
{
    private int bazookaBulletSpeed = 7;
    private float explosionRadius = 1.7f;
    private float damageBoost;
    private FireBullets fireB;
    public readonly float[] damageUpgrades = { 0, 4, 6, 10 };
    public float damage;
    

    private void Start()
    {
        fireB = FindObjectOfType<FireBullets>();
        damageBoost = damageUpgrades[PlayerPrefsManager.Instance.GetBazookaBulletLevel()];
    }

    void Update()
    {
        transform.Translate(0, bazookaBulletSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            Debug.Log("BAZOOKA HIT");
            DamageNearbyEnemies(col);
        }
        fireB.canFire = true;
        Destroy(gameObject);
        //TODO: fix physics layers for collision between bullets 
    }

    public void DamageNearbyEnemies(Collider2D col)
    {
        col.GetComponent<EnemyCore>().TakeDamage(damage);  //we damage the enemy that has collided with the bullet and then all the surrounding enemies
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);  //returns an array with the colliders of the nearby gameobjects    
            foreach (Collider2D enemyCollider in colliders)
            {
                if (enemyCollider.GetComponent<EnemyCore>() != null){ //if enemyCollider isn't null or the particular gameobject is an enemy, damage the enemy
                    enemyCollider.GetComponent<EnemyCore>().TakeDamage(damage + damageBoost);
                }
            }
    }

}
