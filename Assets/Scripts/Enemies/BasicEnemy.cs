﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : EnemyCore {
    public string category;
    public float speed;
    public float damage;
    public float health;
    public float attackSpeed;
    
    // Use this for initialization
    void Awake () {
        Init(category,speed,damage,attackSpeed,health);
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        Attack();
	}
}
